test-wasm:
	emcc -s EXIT_RUNTIME=1 -s ALLOW_MEMORY_GROWTH=1 -s WASM_BIGINT -s EXPORTED_FUNCTIONS='["_main","_malloc","_free"]' \
	-s EXPORTED_RUNTIME_METHODS='["ccall","cwrap"]' -g0 -O3 test.c -o test-wasm.js

test-wincli:
	gcc test.c -o test-wincli

test-wingui:
	gcc -mwindows test.c -o test-wingui

test-linux:
	gcc test.c -o test-linux

test-mac:
	gcc -x objective-c -fobjc-exceptions test.c -o test-mac -framework AppKit
