/*
 * openmodal/test.c
 *
 * Copyright (C) 2023 bzt MIT license
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Test platform native open file modal windows
 *
 */
#include "openmodal.h"

#ifdef __WIN32__
int main(int argc, char **argv);

/* Windows GUI entry point */
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    (void)hInstance;
    (void)hPrevInstance;
    (void)lpCmdLine;
    (void)nCmdShow;
    return main(0, NULL);
}
#endif

int main(int argc, char **argv)
{
    char fn[PATH_MAX];
    uint8_t *data;
    int len = 0;

    (void)argc; (void)argv;


    printf("Loading file data\r\n");
    memset(fn, 0, sizeof(fn));
    data = openmodal_load(fn, &len);
    if(!data) {
        printf("User pressed Cancel\r\n");
    } else {
        printf("Returned %u bytes from file '%s'\r\n", len, fn);
        free(data);
    }

    printf("Saving file data\r\n");
    len = 8;
    data = malloc(len);
    memcpy(data, "Testing", len);
    strcpy(fn, "untitled.txt");
    if(!openmodal_save(fn, data, len)) {
        printf("User pressed Cancel\r\n");
    } else {
        printf("Data successfully saved to '%s'\r\n", fn);
    }
    free(data);
}
