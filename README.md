OpenModal
=========

As usual... I was looking for a simple library to open an open file modal, but couldn't find any. Have you seen
"tinyfiledialogs"? Nearly 8000 SLoC! That's anything but tiny!!! And all those linking requirements of the
"nativefiledialog" library? How is that supposed to be portable?

So I've written my own, suckless, cross-platform, single header ANSI C implementation. It is VERY simple, actually I don't
believe it could be any simpler. It is less than 500 SLoC, and works on Mac, Win, Linux and Emscripten (WebAssembly) too.

API
---

Include `openmodal.h`, that's it. No header nor linking dependencies (ok, under Apple you'll need to link with `-framework AppKit`,
but nothing for Windows, Linux nor Emscripten).

Extremely easy to use, it has only two functions.

```c
uint8_t *openmodal_load(char *name, int *size);
```

Opens the OS native open file modal window, and if the user presses the Cancel button, returns `NULL`. If a file is selected,
then its contents are returned in a newly allocated buffer, with its size returned in `size` (it is the caller's responsibility
to free the buffer). The `name` argument is optional, and if given, then it must be sufficiently large for the returned UTF-8
file name (use PATH_MAX). The name is always returned UTF-8, even on systems that does not use UTF-8 file names.

```c
int openmodal_save(char *name, uint8_t *buf, int size);
```

This one similarily opens the OS native save file modal window, and if the user specifies a file there, it will save `buf` of
`size` bytes into that. If the data successfully written, returns `true`, and the saved file's name in `name` (which might be
different to what was passed, as users can change it on the modal window). The `name` buffer must be sufficiently large for
any possible modifications (use PATH_MAX). If the user pressed Cancel, or there was any other issue with writing the data,
`false` is returned.

That's it. Simple. I like simple.

Implementations
---------------

### Emscripten

Works with lots of hacks. Saving a file cannot be really controlled in a browser environment, and opening a file has its
quirks too.

### Windows

No dependencies. Just standard Windows header files and standard core dlls (`comdlg32.dll` is dynamically linked in run-time).

### Linux

No dependencies. No GTK header files needed, and no GTK linkage nor gtk_main() calls, no nothing. It Just Works (TM).

Do not use `openmodal.h` in your app if you're using GTK yourself, because that might generate some conflict in the GTK
library (that stuff uses static globals, can you believe it??? What kind of library is that?)

### MacOS

In lack of a working Mac computer, I couldn't test this one, but should work. Flying blind I couldn't figure out how to
dynamically link with a framework, but the `-framework AppKit` linking option should be removed too. Contributions are
welcome.

License
-------

Licensed under the permissive MIT. Use as you wish, attribution appreciated.

Cheers,
bzt
